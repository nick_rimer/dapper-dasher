#include <cmath>
#include "raylib.h"

struct Dimensions
{
  const int width;
  const int height;
};

struct AnimationData
{
  Rectangle rectangle;
  Vector2 position;
  float runningTime;
  float updateTime;
  int frameX;
  int frameY;
};

const int NEBULAE_LENGTH{5};
const char *GAME_OVER = "GAME OVER";
const char *YOU_WIN = "YOU WIN";

bool isOnGround(AnimationData data, int windowHeight)
{
  return data.position.y >= windowHeight - data.rectangle.height;
}

AnimationData updateAnimationData(AnimationData data, float deltaTime, int maxFrameX, int maxFrameY, int maxFrameTotal)
{
  data.runningTime += deltaTime;

  if (data.runningTime >= data.updateTime)
  {
    data.rectangle.x = data.frameX * data.rectangle.width;
    data.rectangle.y = data.frameY * data.rectangle.height;
    data.frameX += 1;

    if ((data.frameY * (maxFrameX + 1)) + data.frameX == maxFrameTotal)
    {
      data.frameX = 0;
      data.frameY = 0;
    }
    else if (data.frameX > maxFrameX)
    {
      data.frameX = 0;

      if (data.frameY < maxFrameY)
      {
        data.frameY += 1;
      }
    }

    data.runningTime = 0.0f;
  }

  return data;
}

void drawTextOnScreenCenter(const char *text, int fontSize, Color textColor, Dimensions window)
{
  int textWidth = MeasureText(text, fontSize);
  DrawText(text, (window.width - textWidth) / 2, window.height / 2, fontSize, textColor);
}

int main()
{
  const Dimensions window{512, 380};

  InitWindow(window.width, window.height, "Dapper Dasher");

  const int gravity{1'000};
  const int jumpVelocity{-600};
  const int nebulaVelocity{-300};

  int velocity{};
  bool isInAir{};
  bool collision{};

  Texture2D scarfy = LoadTexture("./assets/scarfy.png");
  Texture2D nebula = LoadTexture("./assets/nebula.png");
  Texture2D background = LoadTexture("./assets/background/back-far.png");
  Texture2D midground = LoadTexture("./assets/background/back-mid.png");
  Texture2D foreground = LoadTexture("./assets/background/back-foreground.png");

  Texture2D backgroundTextures[3]{background, midground, foreground};
  float backgroundPositionsX[3]{0.0f, 0.0f, 0.0f};

  AnimationData scarfyData{
      {0.0f, 0.0f, scarfy.width / 6.0f, (float)scarfy.height},
      {window.width / 2.0f - (scarfy.width / 6.0f) / 2.0f, window.height - (float)scarfy.height},
      0.0f,
      1.0f / 12.0f,
      0,
      0};

  AnimationData nebulae[NEBULAE_LENGTH]{};

  for (int i = 0; i < NEBULAE_LENGTH; i += 1)
  {
    nebulae[i].rectangle = {0.0f, 0.0f, nebula.width / 8.0f, nebula.height / 8.0f};
    nebulae[i].position = {(float)window.width + (500 * i), window.height - nebula.height / 8.0f};
    nebulae[i].runningTime = 0.0f;
    nebulae[i].updateTime = 1.0f / 12.0f;
    nebulae[i].frameX = 0;
    nebulae[i].frameY = 0;
  }

  float finishLine{nebulae[NEBULAE_LENGTH - 1].position.x};

  SetTargetFPS(60);

  while (!WindowShouldClose())
  {
    const float dT{GetFrameTime()};

    BeginDrawing();
    ClearBackground(WHITE);

    for (int i = 0; i < 3; i += 1)
    {
      Texture2D texture = backgroundTextures[i];
      float positionX = backgroundPositionsX[i];
      Vector2 position{positionX, 0.0f};

      Vector2 backgroundPosition1{position.x, 0.0f};
      Vector2 backgroundPosition2{position.x + texture.width * 2, 0.0f};
      DrawTextureEx(texture, backgroundPosition1, 0.0f, 2.0f, WHITE);
      DrawTextureEx(texture, backgroundPosition2, 0.0f, 2.0f, WHITE);

      positionX -= 20 * pow(2, i) * dT;
      if (positionX <= -texture.width * 2)
      {
        positionX = 0.0f;
      }

      backgroundPositionsX[i] = positionX;
    }

    if (isOnGround(scarfyData, window.height))
    {
      isInAir = false;
      velocity = 0;
    }
    else
    {
      isInAir = true;
      velocity += gravity * dT;
    }

    for (AnimationData nebula : nebulae)
    {
      float pad{45.0f};

      Rectangle nebulaRectangle{
          nebula.position.x + pad,
          nebula.position.y + pad,
          nebula.rectangle.width - pad * 2,
          nebula.rectangle.height - pad * 2};

      Rectangle scarfyRectangle{
          scarfyData.position.x,
          scarfyData.position.y,
          scarfyData.rectangle.width,
          scarfyData.rectangle.height};

      if (CheckCollisionRecs(nebulaRectangle, scarfyRectangle))
      {
        collision = true;
      }
    }

    for (int i = 0; i < NEBULAE_LENGTH; i += 1)
    {
      AnimationData nebulaData = nebulae[i];

      nebulaData = updateAnimationData(nebulaData, dT, 7, 7, 61);

      nebulaData.position.x += nebulaVelocity * dT;

      nebulae[i] = nebulaData;
    }

    if (!isInAir)
    {
      scarfyData = updateAnimationData(scarfyData, dT, 5, 0, 6);
    }

    if (collision)
    {
      drawTextOnScreenCenter(GAME_OVER, 40, RED, window);
    }
    else if (scarfyData.position.x >= finishLine)
    {
      drawTextOnScreenCenter(YOU_WIN, 40, GREEN, window);
    }
    else
    {
      for (int i = 0; i < NEBULAE_LENGTH; i += 1)
      {
        DrawTextureRec(nebula, nebulae[i].rectangle, nebulae[i].position, WHITE);
      }

      DrawTextureRec(scarfy, scarfyData.rectangle, scarfyData.position, WHITE);
    }

    if (!isInAir && IsKeyPressed(KEY_SPACE))
    {
      isInAir = true;
      velocity += jumpVelocity;
    }

    scarfyData.position.y += velocity * dT;
    finishLine += nebulaVelocity * dT;

    EndDrawing();
  }

  UnloadTexture(nebula);
  UnloadTexture(scarfy);
  UnloadTexture(background);
  UnloadTexture(midground);
  UnloadTexture(foreground);

  CloseWindow();
}
